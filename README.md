#Your documentation must include:

1. Team member list: BARSHA BARAL (SINGLE TEAM)

2. Device Name: PIXEL XL API 30, ANDROID 11.0

3. Project/App Title: ANDROID MINI APP FINAL

4. Basic instructions on usage: 
AS YOU OPEN ANDROID STUDIO CLICK ON THE PLAY BUTTON, A SCREEN WITH SAVED TO DO LIST WILL APPEAR. YOU CAN ADD MORE TO THE LIST BY CLICKING + SYMBOL AT THE CORNER OF THE PAGE.

5. Any special info we need to run the app: NONE

6. Lessons learned :
This was the first time I used android studio. This project taught me a lot about programing an application and using it. The first thing I learned at this project is how to create layout for your individual page. The xml files were like the html pages/websites where each page had a different layout and styling. That is what I learned to layout for my individual pages. Then I started to code in java. I imported all the classes that were necessary. Android studio was comparatively easier than eclipse. We could import the classes very easily while defining a function. I think wrote codes that connected my layout to the actual functionality of the program. I learned how to create items in recycle view and adding different buttons. 
